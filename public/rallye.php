<html>
<head><title>Stadtrallye Fürth</title></head>
<body>

<?php
session_start();

    $stationsbeschreibung = array(
        1 => "Hier arbeitet der Bürgermeister.", 
        2 => "Hier kann man halb Mensch, halb Pferd im Wasser bestaunen.",
        3 => "1835 landete hier ein Adler.",
        4 => "Der beste Platz für Kinder im Stadtpark.",
        5 => "Drei Gaukler sind hier immer zu Besuch."
        );
    
    if(!isset($_SESSION) { // Wenn diese Seite zum ersten Mal aufgerufen wird
        session_start(); 
        // Drei Session-Variablen registrieren:
        // 1. Benutzername von index.html übernehmen 
            $_SESSION['name'] = $_POST["name"];
        //  2. Nummer der aktuellen Station initialisieren
            $_SESSION['station'] = 0;
        //  3. Ein leeres Array zum speichern der Kontrollzahlen erzeugen
            $kontrollzahlen = array();        
    }
    
    $_SESSION['station']++;

    
    if (isset($_POST['kontrollzahl'])){ // Wenn die Variable 'kontrollzahl' übergeben wurde
        $_SESSION['kontrollzahlen'][$station] = $_POST['kontrollzahl'];
    }
    
    // Wenn noch nicht alle Stationen gefunden wurden
    if ($_SESSION['station'] < sizeof($stationsbeschreibung) {     
        
        //Wo bekomme ich $name, $station und $beschreibung für die folgende Ausgabe her???
        // ^von der  session
        $name = ;
        $station = "$_SESSION['station']";
        $beschreibung = "$_SESSION['stationsbeschreibung']";
        echo "<p>Hallo " . $_SESSION['name'] . ", Deine nächste Station mit der Nummer $station hat folgende Beschreibung:</p><p><i>$beschreibung</i></p>";
        echo "<p>Bitte gib die Kontrollzahl für Station $station ein!</p>";
        
        //Hier fehlt ein Formular, um die Kontrollzahl eingeben und abschicken zu können.
        echo "<input type = 'number' name = 'Kontrollzahl' placeholder = 'Nummer'>";
    
    } else {
        
        echo "<p>Glückwunsch, $name!</p><p>Du hast alle Stationen gefunden. Wenn alle Kontrollzahlen richtig sind, nimmst Du am Gewinnspiel teil!</p>";
        echo "<p>Hier nochmal Deine Kontrollzahlen:</p>";
        
        //Hier müssen die gesammelten Kontrollzahlen ausgegeben werden!
        for ($i = 1; $i <= sizeof(sizeof($kontrollzahlen); $i++) {
            echo "<ul>";
            echo "<li> </li>";
            echo "</ul>";
        }
    }


?>
</body>
</html>
